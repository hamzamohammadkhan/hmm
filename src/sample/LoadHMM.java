package sample;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;

public class LoadHMM {

    private static final String FILENAME = "E:\\java programs\\hmm\\src\\sample\\probabilites.txt";

    String[] myList = new String[10];

    public void fileloader ()
    {
        BufferedReader br = null;
        FileReader fr = null;
        int c=0;

        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(FILENAME);
            br = new BufferedReader(fr);

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                myList[c]=sCurrentLine;
                c++;
            }

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }

        int ns=c-2;

        System.out.println("num of states: "+ ns);


        float[][] transProbab = new float[ns][ns];
        for (int i = 0; i < ns; i++) {
            for (int j = 0; j < ns; j++) {
                String[] output = myList[i].split(",");
                transProbab[i][j] = Float.parseFloat((output[j]));
            }
        }

        System.out.println(myList[ns]);
        System.out.println(myList[ns+1]);

        float[] initProbab = new float[ns];
        for (int i = 0; i < ns; i++) {

                String[] output = myList[ns].split(",");
                initProbab[i] = Float.parseFloat((output[i]));

        }

        float[] emitProbab = new float[ns];
        for (int i = 0; i < ns; i++) {

            String[] output = myList[ns+1].split(",");
            emitProbab[i] = Float.parseFloat((output[i]));

        }

        System.out.println("trans ");

        for (int i = 0; i < ns; i++) {
            for (int j = 0; j < ns; j++) {
                System.out.println(transProbab[i][j]);

            }
        }

        System.out.println("emit ");
        for (int i = 0; i < ns; i++) {

            System.out.println(emitProbab[i]);
        }

        System.out.println("init ");
        for (int i = 0; i < ns; i++) {

            System.out.println(initProbab[i]);
        }


    }
}
