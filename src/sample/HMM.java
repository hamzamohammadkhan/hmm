package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class HMM extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args)
    {
        //SequenceGenerator s = new SequenceGenerator();
        //s.randwala();
        LoadHMM l = new LoadHMM();
        l.fileloader();
        launch(args);
    }

    public void forward ()
    {

    }

    public void viterbi ()
    {

    }

    public void fwdbwd ()
    {

    }

    public void trainHMM ()
    {

    }

    public void testHMM ()
    {

    }

}
